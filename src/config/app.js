const multer = require('multer');
const path = require('path');

module.exports = {
    dbHost: "localhost",
    dbDatabase: "aktielib",
    jwtHash: "4a5407546681412407054a275376aa5d",
    multer: {
        dest: path.resolve(__dirname, '..', '..', 'uploads'),
        storage: multer.diskStorage({
            destination: (request, file, callback) => {
                callback(null, path.resolve(__dirname, '..', '..', 'uploads'));
            },
            filename: (request, file, callback) => {
                const fileName = `${ Math.random() }_${ file.originalname }`;
                callback(null, fileName);
            }
        }),
        limits: {
            fileSize: 3 * 1024 * 1024, // 3MB
        },
        fileFilter: (request, file, callback) => {
            
            const allowedFormats = [
                'image/jpeg',
                'image/png',
                'image/gif'
            ];

            if( allowedFormats.includes( file.mimetype ) ){
                callback(null, true);
            }else{
                callback(new Error('Invalid file format.'));
            }

        }
    }
}