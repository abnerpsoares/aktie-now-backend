const express    = require('express');
const bodyParser = require('body-parser');
const cors       = require('cors');

const app = express();

app.use(cors()); // permite requisições via localhost
app.use(bodyParser.json()); // "traduz" requisições em formato json
app.use(bodyParser.urlencoded({ extended: false })); // "traduz" parâmetros via url

// inicializa o controller Auth
require('./app/controllers/authController')(app);

// inicializa o controller Book
require('./app/controllers/bookController')(app);

app.listen(3000);