const mongoose = require('../../database');
const bcrypt   = require('bcryptjs');

const BacknedUserSchema = new mongoose.Schema({

    name: {
        type: String,
        require: true
    },
    user: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    created_at: {
        type: Date,
        default: Date.now
    }

});

BacknedUserSchema.pre('save', async function (next) {

    // boforeSave -> encripta a senha utilizando o bcrypt
    const hash = await bcrypt.hash(this.password, 5);
    this.password = hash;

    next();

});

const BackendUser = mongoose.model('BackendUser', BacknedUserSchema);

module.exports = BackendUser;