const mongoose = require('../../database');

const BookSchema = new mongoose.Schema({

    backenduser: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'BackendUser',
        required: true
    },
    ratings: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Rating'
    }],
    name: {
        type: String,
        require: true
    },
    author: {
        type: String,
        required: true
    },
    release_year: {
        type: Number,
        required: true
    },
    summary: {
        type: String,
        required: false
    },
    cover_image: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    modified_at: {
        type: Date,
        default: Date.now
    }
    
});

const Book = mongoose.model('Book', BookSchema);

module.exports = Book;