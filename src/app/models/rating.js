const mongoose = require('../../database');

const RatingSchema = new mongoose.Schema({

    book: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Book',
        required: true
    },
    person_name: {
        type: String,
        require: true
    },
    stars: {
        type: Number,
        required: true
    },
    ip_address: {
        type: String,
        require: false
    },
    created_at: {
        type: Date,
        default: Date.now
    },
    modified_at: {
        type: Date,
        default: Date.now
    }
    
});

const Rating = mongoose.model('Rating', RatingSchema);

module.exports = Rating;