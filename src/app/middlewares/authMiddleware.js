const jwt    = require('jsonwebtoken');
const config = require('../../config/app');

freeRouteAccess = ( request ) => {

    // ::: Rotas livres de autenticação :::
    //  (GET) /book -> lista dos livros
    //  (POST) /book/rating -> cria avaliação do usuário
    //  (GET) /book/cover/:bookId -> retorna a imagem de capa do livro

    if( (request.originalUrl == '/book' && request.method == 'GET') || 
        (request.originalUrl == '/book/rating' && request.method == 'POST') ||
        (request.originalUrl.startsWith('/book/cover') && request.method == 'GET')
    ){
        return true;
    }else{
        return false;
    }

}

module.exports = (request, response, next) => {
    
    // verifica se a rota necessita ou não de autorização
    if( freeRouteAccess(request) ){
        return next();
    }

    // recupera a autenticação do header
    const authHeader = request.headers.authorization;

    // se não foi informado o token de autenticação, retorna erro
    if( !authHeader ){
        return response.status(401).send({ 'error': 'Token not found.' });
    }

    // De acordo com o formato da autenticação (Bearer: token)
    // verifica se a string token contém o formato correto.
    // Se não, retorna erro
    const parts = authHeader.split(' ');
    if( parts.length !== 2 ){
        return response.status(401).send({ 'error': 'Invalid token [1].' });
    }
    
    // Verifica se o scheme do token esta correto.
    // Se não, retorna erro
    const [ scheme, token ] = parts;
    if( !/^Bearer$/i.test(scheme) ){
        return response.status(401).send({ 'error': 'Invalid token [2].' });
    }

    // Após o token passar por todas as validações básicas,
    // verifica se de fato o token está correto & válido.
    jwt.verify( token, config.jwtHash, (error, decoded) => {

        // Se estiver expirado ou incorreto,
        // retorna erro
        if( error ){
            return response.status(401).send({ 'error': 'Invalid token [3].' })
        }

        // Passa o ID do backenduser para o request seguinte
        request.backenduserId = decoded.id;

        return next();

    });

};