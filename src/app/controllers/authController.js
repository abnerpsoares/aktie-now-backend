const express = require('express');
const bcrypt  = require('bcryptjs');
const jwt     = require('jsonwebtoken');
const config  = require('../../config/app');

const router = express.Router();

const BackendUser = require('../models/backenduser');

// endpoint usado apenas para criar usuários para testes Aktie Now
router.post('/secretregister', async(request, response) => {

    const { user } = request.body;

    try {
        
        // verifica se usuário existe
        // se sim, retorna erro de usuário existente
        if( await BackendUser.findOne({ user }) ){
            return response.status(400).send({ 'error': 'User already exists.' });
        }

        // cria o usuário de acordo com os parâmetros recebidos
        const backenduser = await BackendUser.create( request.body );

        // unset no password
        backenduser.password = undefined;

        // retorna o objeto do usuário criado
        return response.send({ backenduser });

    }
    catch( error ){
        return response.status(400).send(error);
    }

});

// endpoint para autenticação do usuário & recuperação do token
router.post('/authenticate', async(request, response) => {

    const { user, password } = request.body;

    // busca o backenduser trazendo o password para futura comparação
    // (definido como `select:false` na model)
    const backenduser = await BackendUser.findOne({ user }).select('+password');

    // se o usuário não existir, retorna erro
    if( !backenduser ){
        return response.status(400).send({ 'error': 'User not found.' });
    }

    // se a senha estiver inválida, retorna erro
    if( !await bcrypt.compare(password, backenduser.password) ){
        return response.status(400).send({ 'error': 'Invalid password.' });
    }

    // unset no password
    backenduser.password = undefined;

    // gera o token com prazo de expiração = 4 horas
    const token = jwt.sign({ 'id': backenduser.id }, config.jwtHash, {
        expiresIn: 14400
    });

    // retorna o objeto backenduser & o token
    response.send({ backenduser, token });
    
});

module.exports = (app) => { app.use('/auth', router) };