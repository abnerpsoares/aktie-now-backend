const express        = require('express');
const multer         = require('multer');
const config         = require('../../config/app');
const authMiddleware = require('../middlewares/authMiddleware');

const router = express.Router();

const Book   = require('../models/book');
const Rating = require('../models/rating');

// utiliza o middleware de autenticação
router.use( authMiddleware );

// endpoint para retornar lista de livros
router.get('/', async (request, response) => {

    try {

        const books = await Book.find().populate(['backenduser', 'ratings']);
        return response.send({ books });

    }
    catch( error ){
        return response.status(400).send({ 'error': 'Error loading the book list.' });
    }

});

// endpoint para retornar detalhes de um objeto livro
router.get('/:bookId', async (request, response) => {

    try {

        const book = await Book.findById( request.params.bookId ).populate(['backenduser', 'ratings']);

        if( !book ){
            return response.status(400).send({ 'error': 'Book not found.' });
        }

        return response.send({ book });

    }
    catch( error ){
        return response.status(400).send({ 'error': 'Error loading the book.' });
    }

});

// endpoint para cadastrar um novo livro
// :: utilizado o middleware multer para o upload da capa do livro
router.post('/', multer(config.multer).single('cover_image'), async (request, response) => {

    try {

        const createBook = await Book.create({ ...request.body, cover_image: request.file.filename, backenduser: request.backenduserId });
        return response.send({ book: createBook });

    }
    catch( error ){
        return response.status(400).send({ 'error': 'Error creating the book.' });
    }

});

// endpoint para editar um livro
// :: utilizado o middleware multer para o upload da capa do livro
router.put('/:bookId',  multer(config.multer).single('cover_image'), async (request, response) => {

    try {

        const book = await Book.findById( request.params.bookId );
        if( !book ){
            return response.status(400).send({ 'error': 'Book not found.' });
        }

        const { name, author, release_year, summary } = request.body;

        const updateBook = await Book.findByIdAndUpdate(request.params.bookId, {
            name,
            author,
            release_year,
            summary,
            cover_image: request.file !== undefined ? request.file.filename : book.cover_image,
            modified_at: Date.now()
        }, { new: true });

        return response.send({ book: updateBook });

    }
    catch( error ){
        return response.status(400).send({ 'error': 'Error editing the book.' });
    }

});

// endpoint para remover um livro
router.delete('/:bookId', async (request, response) => {

    try {

        const book = await Book.findById( request.params.bookId );
        if( !book ){
            return response.status(400).send({ 'error': 'Book not found.' });
        }

        // remove todas as avalições associadas ao livro
        await Rating.remove({ book: request.params.bookId });
        
        // remove o livro
        await Book.findByIdAndRemove( request.params.bookId );
        
        return response.send({ 'success': 'Book deleted.' });

    }
    catch( error ){
        return response.status(400).send({ 'error': 'Error deleting the book.' });
    }

});

// endpoint para dar notas à um livro
router.post('/rating', async (request, response) => {

    try {

        const { person_name, description, stars, bookId } = request.body;

        const book = await Book.findById( bookId ).populate('ratings');
        if( !book ){
            return response.status(400).send({ 'error': 'Book not found.' });
        }

        const createRating = await Rating.create({ 
            person_name,
            description,
            stars,
            book: bookId
        });

        // após a criação da avaliação,
        // adiciona o relacionamento com a model Book.
        book.ratings.push( createRating );
        await book.save();

        return response.send({ rating: createRating });

    }
    catch( error ){
        return response.status(400).send({ 'error': 'Error creating the rating.' });
    }

});

// endpoint para retornar a imagem de capa
router.get('/cover/:bookId', async (request, response) => {

    const book = await Book.findById( request.params.bookId );
    if( !book ){
        return response.status(404).send({ 'error': 'Book not found.' });
    }

    return response.sendFile( `${ config.multer.dest }/${ book.cover_image }` );

});

module.exports = (app) => { app.use('/book', router) };;