const mongoose = require('mongoose');
const config   = require('../config/app');

mongoose.connect(`mongodb://${config.dbHost}/${config.dbDatabase}`);
mongoose.Promise = global.Promise;

module.exports = mongoose;