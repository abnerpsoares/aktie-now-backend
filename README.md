# AKTIElib. - Backend

Backend do sistema de avaliações de livros desenvolvido em NodeJs + Express + MongoDB (APIs Restful).

# Instalação:
  - O bom e velho ```npm install``` haha.

# Features:
  - Criar usuários backend (para testes);
  - Autenticar e recuperar o token de um usuário (jwt & bcryptjs);
  - Verificação se o usuário tem permissão ou não para executar uma ação (esta logado?);
  - CRUD dos livros com upload de imagens;
  - Inserir avaliações dos livros.

# Requests Disponíveis:

*POST* /auth/secretregister
    ```{
    	"name": "Abner Soares",
    	"user": "abner",
    	"password": "123456"
    }```

*POST* /book
    ```{
    	"name": "O Conde de Monte Cristo",
    	"author": "Alexandre Dumas",
    	"release_year": 1844,
    	"summary": "Publicado em 1844, “O Conde de Monte Cristo é, juntamente com “Os Três Mosqueteiros”, a obra mais conhecida de Alexandre Dumas e uma das mais celebradas da literatura universal. O livro narra a história de um marinheiro que foi preso injustamente. Quando escapa da prisão, e toma posse de uma misteriosa fortuna e arma uma plano para vingar-se daqueles que o prenderam. Estima-se que tenha vendido entre 200 e 250 milhões de cópias.",
    	"cover_image": file
    }```
    
*PUT* /book/:bookId
    ```{
        	"name": "O Conde de Monte Cristo",
        	"author": "Alexandre Dumas",
        	"release_year": 1844,
        	"summary": "Publicado em 1844, “O Conde de Monte Cristo é, juntamente com “Os Três Mosqueteiros”, a obra mais conhecida de Alexandre Dumas e uma das mais celebradas da literatura universal. O livro narra a história de um marinheiro que foi preso injustamente. Quando escapa da prisão, e toma posse de uma misteriosa fortuna e arma uma plano para vingar-se daqueles que o prenderam. Estima-se que tenha vendido entre 200 e 250 milhões de cópias.",
        	"cover_image": file
    }```
    
*POST* /book/rating
    ```{
    	"bookId": "5d959c65aaa8a11dc6d830d9",
    	"person_name": "Abner Soares",
    	"stars": 5
    }```
    
*GET* /book

*GET* /book/:bookId

*DELETE* /book/:bookId









Valeuuu ;)
